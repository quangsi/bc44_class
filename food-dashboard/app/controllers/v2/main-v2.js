// fetch data từ server và render

import { Food } from "../../models/v1/model.js";
import { layThongTinTuForm } from "../v1/controller-v1.js";
import { renderFooList } from "./controlder-v2.js";

const BASE_URL = "https://633ec05b0dbc3309f3bc5455.mockapi.io/food";

// true chay và còn
// false mặn và hết
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      // thiếu method tinhGiaKM
      let foodArr = res.data.map((item) => {
        //destructuring
        let { name, type, discount, img, desc, price, status, id } = item;

        // new object
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFooList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

let xoaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;

fetchFoodList();

window.themMon = () => {
  console.log("yes them mon");
  let data = layThongTinTuForm();
  console.log("🚀 - file: main-v2.js:53 - data", data);
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.suaMonAn = (id) => {
  // lấy chi tiết 1 món ăn từ id
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      // show data lên form
      let { id, type, price, img, status, desc, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {
      console.log(err);
    });
};
