// map

// forEach

export let renderFooList = (foodArr) => {
  console.log("🚀 - file: controlder-v2.js:6 - foodArr", foodArr);
  let contentHTML = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, khuyenMai, tinhTrang, giaMon } = item;
    let contentTr = `<tr>
        <td>${maMon}</td>
        <td>${tenMon}</td>
        <td>${loai ? "Chay" : "Mặn"}</td>
        <td>${giaMon}</td>
        <td>${khuyenMai}</td>
        <td>${item.tinhGiaKM()}</td>
        <td>${tinhTrang ? "Còn" : "Hết"}</td>
        <td>
        <button 
        onclick="suaMonAn(${maMon})"
         class="btn btn-primary">Edit</button>
        <button 
        onclick="xoaMonAn(${maMon})"
        class="btn btn-danger">Delete</button>
        </td>
  </tr>`;
    contentHTML += contentTr;
  });
  // reduce
  // tbodyFood
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

let colors = ["red", "green", "blue", "yellow"];

colors.forEach((item, index) => {
  console.log("heere", item, index);
});
let newColor = colors.map((item) => {
  // return "the " + item;
});
console.log("🚀 - file: controlder-v2.js:23 - newColor", newColor);

// anki app
