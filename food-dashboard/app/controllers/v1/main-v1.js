// import { username, age } from "./controller-v1.js";

import { Food } from "../../models/v1/model.js";
import hienThiThongTin, { layThongTinTuForm } from "./controller-v1.js";

// import sayGoodbye from "./controller-v1.js";

// console.log(username);
// console.log(age);

// sayGoodbye();

function themMon() {
  let data = layThongTinTuForm();
  let { maMon, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    data;
  let food = new Food(
    maMon,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  hienThiThongTin(food);
  //
}

window.themMon = themMon;
